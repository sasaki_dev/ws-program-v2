/**
 * Created by siqi on 4/9/16.
 */
var config = {};

//use for faculty and staff
config.colors = d3.map(
    {
        'red':'#F05A28',
        'blue':'#03afeb',
        'green':'#00B273',
        'purple':'#B41E87',
        'navy':'#213F99',
        'celadon':'#46BFB0',
        'gray':'rgb(230,230,230)',
        'gray2':'rgb(150,150,150)',
        'yellow':'#ffdd00',
        'orange':'#F15B29',
        'pink':'#F075AD',
        'darkgray':'rgb(50,50,50)'
    }
);

//use for space planning
config.updatedcolors = d3.map(
    {
            'yellow':'#EFD01A',
            'blue':'#1672B6',
            'lightblue':'#3CB1D5',
            'orange':'#F15B29',
            'purple':'#7260AA',
            'pink':'#F075AD',
            'darkpink':'#EB4195',
            'darkgray':'rgb(50,50,50)',
            'mediumgray' : 'rgb(150,150,150)',
            'lightgray' : 'rgb(225,225,225)'

    }
);

config.colorByFicm = d3.scale.ordinal()
    .domain([0,100,200,300,400,500,600,700,800])
    .range([
        config.updatedcolors.get('lightgray'), //0
        config.updatedcolors.get('blue'), //100
        config.updatedcolors.get('lightblue'), //200
        config.updatedcolors.get('orange'), //300
        config.updatedcolors.get('purple'), //400
        config.updatedcolors.get('darkpink'), //500
        config.updatedcolors.get('darkpink'), //600
        config.updatedcolors.get('mediumgray'), //700
        config.updatedcolors.get('yellow') //800
    ]);


//config.colorByFicm = d3.scale.ordinal()
//    .domain([0,100,200,300,400,500,600,700,800])
//    .range([
//        config.colors.get('gray'),
//        config.colors.get('blue'),
//        config.colors.get('lightblue'),
//        config.colors.get('orange'),
//        config.colors.get('purple'),
//        config.colors.get('pink'),
//        config.colors.get('darkpink'),
//        config.colors.get('gray'),
//        config.colors.get('yellow'),
//    ]);

config.colorByDivision = d3.scale.ordinal()
    .domain([
        'IA', //eng & arch
        'AE', //student life
        'SH', //soc and humanities
        'NG', //biz
        'PR', //prepa
        'UV', //virtual
        'NA', //none
        'GA'])
    .range([
        config.colors.get('green'),
        config.colors.get('gray2'),
        config.colors.get('navy'),
        config.colors.get('red'),
        config.colors.get('celadon'),
        config.colors.get('blue'),
        config.colors.get('gray'),
        config.colors.get('yellow')
    ]);