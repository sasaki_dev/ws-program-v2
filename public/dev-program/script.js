console.log(d3);

var m = {t:200,r:100,b:200,l:100},
	w = d3.select('.program').node().clientWidth - m.l - m.r,
	h = d3.select('.program').node().clientHeight - m.t - m.b;

var svg = d3.select('.program')
	.append('svg').attr({
		width:w+m.l+m.r,
		height:h+m.t+m.b
	});
var plot = svg
	.append('g').attr('transform','translate('+m.l+','+m.t+')');


var programsByBlock = [], //array that holds dev data from csv
	programToId = d3.map(),
	blockToId = d3.map(),
	p = 0, b = 0;

var newProgram = d3.map(); //map holds update values for unbuilt programs

var layoutConfig = 'block';

var scaleY = d3.scale.linear().domain([0,1000000]).range([h,0]),
	scaleColor = d3.scale.ordinal().range(config.colors.values());

var axisY = d3.svg.axis()
	.orient('right')
	.tickSize(w)
	.scale(scaleY);

d3.csv('../00_data/ws-program-mepa.csv',parse,dataloaded);

function dataloaded(err, rows){
	console.log(programToId);
	console.log(blockToId);
	console.log(programsByBlock);

	//turn program table into crossfilter to enable easy sorting
	var pTable = crossfilter(programsByBlock);
	var pTableByBlock = pTable.dimension(function(d){return d.block});
	var pTableByProgram = pTable.dimension(function(d){return d.program});

	//Draw static elements
	scaleColor.domain(programToId.keys());
	plot.append('g')
		.attr('class','axis axis-y')
		.call(axisY);
	var legendItems = svg.append('g').attr('transform','translate('+m.l+',100)')
		.selectAll('.legend-item')
		.data(programToId.keys())
		.enter()
		.append('g').attr('class','legend-item').attr('transform',function(d,i){return 'translate('+i*100+',0)'});
	legendItems.append('rect')
		.attr('width',20)
		.attr('height',10)
		.style('fill', function(d){return scaleColor(d)});
	legendItems.append('text')
		.text(function(d){return d})
		.attr('x', 25)
		.attr('dy', 10);


	//Control buttons switch layout
	d3.select('.control').selectAll('.btn')
		.on('click',function(d){
			layoutConfig = d3.select(this).attr('id');
			redraw(layoutConfig);
		});

	//initialize dat.gui
	var gui = new dat.GUI();

	var blocks = ['N','P','L3','L4','L5','L6','D','G'];
	blocks.forEach(function(b){

		pTableByBlock.filter(b);

		var max = 500000;
		var program = new function(){
			this.Retail = (pTableByProgram.filter('Retail').top(Infinity)[0].v);
			this.Residential = (pTableByProgram.filter('Residential').top(Infinity)[0].v);
			this.Office = (pTableByProgram.filter('Office').top(Infinity)[0].v);
			this.Hotel = (pTableByProgram.filter('Hotel').top(Infinity)[0].v);
			this.Parking = (pTableByProgram.filter('Parking').top(Infinity)[0].v);
			this.Education = (pTableByProgram.filter('Education').top(Infinity)[0].v);
		};

		newProgram.set(b, program);

		var folder = gui.addFolder('Block '+b);
		folder.add(program,'Retail',0,max).step(1000).onFinishChange(updateProgram);
		folder.add(program,'Residential',0,max).step(1000).onFinishChange(updateProgram);
		folder.add(program,'Office',0,max).step(1000).onFinishChange(updateProgram);
		folder.add(program,'Hotel',0,max).step(1000).onFinishChange(updateProgram);
		folder.add(program,'Education',0,max).step(1000).onFinishChange(updateProgram);
		folder.add(program,'Parking',0,500).step(10).onFinishChange(updateProgram);

	});

	function updateProgram(value){
		//updated program stored in newProgram map
		//existing programs in programsByBlock

		newProgram.forEach(function(block, value){
			pTableByBlock.filter(block);

			pTableByProgram.filter('Retail').top(Infinity)[0].v = value.Retail;
			pTableByProgram.filter('Residential').top(Infinity)[0].v = value.Residential;
			pTableByProgram.filter('Office').top(Infinity)[0].v = value.Office;
			pTableByProgram.filter('Hotel').top(Infinity)[0].v = value.Hotel;
			pTableByProgram.filter('Education').top(Infinity)[0].v = value.Education;
			pTableByProgram.filter('Parking').top(Infinity)[0].v = value.Parking;
		});

		redraw(layoutConfig);
	}

	function redraw(_l){
		if(_l == "program"){ scaleY.domain([0,6500000])}
		else{ scaleY.domain([0,1000000])}

		(recomputeLayout(_l))();

		plot.select('.axis-y').transition().duration(1000).call(axisY);
	}

	function recomputeLayout(layout){
		//@param layout --> x axis variable, "block"||"program"
		return function(){
			var layers = d3.nest().key(function(d){
					return layout=="program"?d.block:d.program; //y axis layers
				})
				.entries(programsByBlock);
			var map = layout=="block"?blockToId:programToId;
			var stack = d3.layout.stack()
				.values(function(d){return d.values})
				.x(function(d){return map.get(d[layout])})
				.y(function(d){return d.v});

			//draw & update program blocks
			stack(layers).forEach(function(layer){
				var node = plot.selectAll('.program')
					.data(layer.values,function(d){return d.block+'-'+d.program});

				var nodeEnter = node.enter()
					.append('g').attr('class','program')
					.on('click',function(d){
						if(!d.selected){
							d.selected = true;
							d3.select(this).select('.under').style('stroke-opacity',.5);
						}else{
							d.selected = false;
							d3.select(this).select('.under').style('stroke-opacity',.05);
						}
					});
				nodeEnter.append('line').attr('class','under')
					.style('fill','none')
					.style('stroke',function(d){
						return scaleColor(d.program);
					})
					.style('stroke-width','30px')
					.style('stroke-opacity',.05);
				nodeEnter.append('line').attr('class','over')
					.style('fill','none')
					.style('stroke',function(d){
						return scaleColor(d.program);
					})
					.style('stroke-width','3px');			


				var nodeTransition = node
					.transition()
					.duration(1000)
					.attr('transform', function(d){
						return 'translate('+ (map.get(d[layout])+1)*w/(map.keys().length+1) +', 0)';
					});
				nodeTransition.select('.under')
					.attr('y1',function(d){return scaleY(d.y0)})
					.attr('y2',function(d){return scaleY(d.y0+d.y)});
				nodeTransition.select('.over')
					.attr('y1',function(d){return scaleY(d.y0)})
					.attr('y2',function(d){return scaleY(d.y0+d.y)});
			});

			//draw and update labels
			var labels = plot.selectAll('.label')
				.data(map.entries(), function(d,i){return i});

			labels.enter().append('text')
				.attr('class','label')
				.attr('text-anchor','middle')
			labels.exit().remove();

			labels.text(function(d){return d.key})
				.attr('y', h+20)
				.transition()
				.attr('x',function(d){
					return (d.value+1)*w/(map.keys().length+1)
				})
		}
	}


}

function parse(d){
	var block = d.Block;
	delete d.Block;

	if(!blockToId.get(block)){
		blockToId.set(block,b);
		b++;
	}

	for(key in d){
		programsByBlock.push({
			block: block,
			program: key,
			v: +d[key]
		});

		if(programToId.get(key)==undefined){
			programToId.set(key,p);
			p++;
		}
	}
}
