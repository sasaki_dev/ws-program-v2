console.log(d3);

var chartProps = {
    m: "",
    w: "",
    h: "",
    svg: "",
    plot: "",
    axisY: "",
    programsByBlock: "",
    programToId: "",
    blockToId: "",
    p: "",
    b: "",
    newProgram: "",
    scaleY: "",
    scaleColor: "",
    pTable: "",
    pTableByBlock: "",
    pTableByProgram: ""
}

function blocksChart() {
    var bp = chartProps;
    bp.m = {t: 200, r: 100, b: 200, l: 100},
        bp.w = d3.select('.blocks').node().clientWidth - bp.m.l - bp.m.r,
        bp.h = d3.select('.blocks').node().clientHeight - bp.m.t - bp.m.b;

    bp.svg = d3.select('.blocks')
        .append('svg').attr({
            width: bp.w + bp.m.l + bp.m.r,
            height: bp.h + bp.m.t + bp.m.b
        });

    bp.plot = bp.svg
        .append('g').attr('transform', 'translate(' + bp.m.l + ',' + bp.m.t + ')');

    setupArrays(bp);

    bp.axisY = d3.svg.axis()
        .orient('right')
        .tickSize(bp.w)
        .scale(bp.scaleY);

    d3.csv('../00_data/ws-program-mepa.csv', parse, dataloaded, finished(bp));
    // drawLegend();
    redraw(bp);
    //startGUI(bp)
}


var finished = function (bp, callback) {
    console.log('we are here')
    setupCrossFilter(bp);
    drawStaticElements(bp);
    redraw(bp);
    var layout = "block";
    recomputeLayout(bp, layout);
};

blocksChart();

function setupArrays(bp) {
    bp.programsByBlock = [], //array that holds dev data from csv
        bp.programToId = d3.map(),
        console.log(bp.programToId);
    bp.blockToId = d3.map(),
        bp.p = 0, bp.b = 0;

    bp.newProgram = d3.map(); //map holds update values for unbuilt programs
    bp.scaleY = d3.scale.linear().domain([0, 1000000]).range([bp.h, 0]),
        bp.scaleColor = d3.scale.ordinal().range(config.colors.values());
}

function setupCrossFilter(bp) {
    //turn program table into crossfilter to enable easy sorting
    console.log("setup");
    bp.pTable = crossfilter(programsByBlock);
    bp.pTableByBlock = bp.pTable.dimension(function (d) {
        return d.block
    });
    bp.pTableByProgram = bp.pTable.dimension(function (d) {
        return d.program
    });
}

function drawLegend() {
    var legendItems = blocksChart.svg.append('g').attr('transform', 'translate(' + m.l + ',100)')
        .selectAll('.legend-item')
        .data(programToId.keys())
        .enter()
        .append('g').attr('class', 'legend-item').attr('transform', function (d, i) {
            return 'translate(' + i * 100 + ',0)'
        });
    legendItems.append('rect')
        .attr('width', 20)
        .attr('height', 10)
        .style('fill', function (d) {
            return scaleColor(d)
        });
    legendItems.append('text')
        .text(function (d) {
            return d
        })
        .attr('x', 25)
        .attr('dy', 10);
}

function drawStaticElements(bp) {
    //Draw static elements
    bp.scaleColor.domain(bp.programToId.keys());
    bp.plot.append('g')
        .attr('class', 'axis axis-y')
        .call(bp.axisY);
}

function redraw(bp) {
    bp.scaleY.domain([0, 1000000]);
    (recomputeLayout(layout))();

    bp.plot.select('.axis-y').transition().duration(1000).call(bp.axisY);
    console.log('redraw done');
}

function dataloaded(err, rows) {

    console.log(bp.programToId);
    console.log(bp.blockToId);
    console.log(bp.programsByBlock);

    function recomputeLayout(bp, layout) {
        //@param layout --> x axis variable, "block"||"program"
        return function () {
            var layers = d3.nest().key(function (d) {
                    return d.program; //y axis layers
                })
                .entries(bp.programsByBlock);
            var map = bp.blockToId;
            var stack = d3.layout.stack()
                .values(function (d) {
                    return d.values
                })
                .x(function (d) {
                    return map.get(d[layout])
                })
                .y(function (d) {
                    return d.v
                });

            //draw & update program blocks
            stack(layers).forEach(function (layer) {

                if (layout == "block") {
                    var node = bp.plot.selectAll('.blocks')
                        .data(layer.values, function (d) {
                            return d.block + '-' + d.program
                        });

                    var nodeEnter = node.enter()
                        .append('g').attr('class', 'blocks');
                }
                else {
                    var node = bp.plot.selectAll('.program')
                        .data(layer.values, function (d) {
                            return d.block + '-' + d.program
                        });

                    var nodeEnter = node.enter()
                        .append('g').attr('class', 'program');

                }

                nodeEnter.on('click', function (d) {
                    if (!d.selected) {
                        d.selected = true;
                        d3.select(this).select('.under').style('stroke-opacity', .5);
                    } else {
                        d.selected = false;
                        d3.select(this).select('.under').style('stroke-opacity', .05);
                    }
                });
                nodeEnter.append('line').attr('class', 'under')
                    .style('fill', 'none')
                    .style('stroke', function (d) {
                        return scaleColor(d.program);
                    })
                    .style('stroke-width', '30px')
                    .style('stroke-opacity', .05);
                nodeEnter.append('line').attr('class', 'over')
                    .style('fill', 'none')
                    .style('stroke', function (d) {
                        return scaleColor(d.program);
                    })
                    .style('stroke-width', '3px');


                var nodeTransition = node
                    .transition()
                    .duration(1000)
                    .attr('transform', function (d) {
                        return 'translate(' + (map.get(d[layout]) + 1) * w / (map.keys().length + 1) + ', 0)';
                    });
                nodeTransition.select('.under')
                    .attr('y1', function (d) {
                        return scaleY(d.y0)
                    })
                    .attr('y2', function (d) {
                        return scaleY(d.y0 + d.y)
                    });
                nodeTransition.select('.over')
                    .attr('y1', function (d) {
                        return scaleY(d.y0)
                    })
                    .attr('y2', function (d) {
                        return scaleY(d.y0 + d.y)
                    });
            });

            //draw and update labels
            var labels = bp.plot.selectAll('.label')
                .data(map.entries(), function (d, i) {
                    return i
                });

            labels.enter().append('text')
                .attr('class', 'label')
                .attr('text-anchor', 'middle')
            labels.exit().remove();

            labels.text(function (d) {
                    return d.key
                })
                .attr('y', bp.h + 20)
                .transition()
                .attr('x', function (d) {
                    return (d.value + 1) * bp.w / (map.keys().length + 1)
                })
        }

    }

}

function startGUI(bp) {

    //initialize dat.gui
    var gui = new dat.GUI();

    var blocks = ['N', 'P', 'L3', 'L4', 'L5', 'L6', 'D', 'G'];
    blocks.forEach(function (bp, b) {

        bp.pTableByBlock.filter(b);

        var max = 500000;
        var program = new function (bp) {
            this.Retail = (bp.pTableByProgram.filter('Retail').top(Infinity)[0].v);
            this.Residential = (bp.pTableByProgram.filter('Residential').top(Infinity)[0].v);
            this.Office = (bp.pTableByProgram.filter('Office').top(Infinity)[0].v);
            this.Hotel = (bp.pTableByProgram.filter('Hotel').top(Infinity)[0].v);
            this.Parking = (bp.pTableByProgram.filter('Parking').top(Infinity)[0].v);
            this.Education = (bp.pTableByProgram.filter('Education').top(Infinity)[0].v);
        };

        newProgram.set(b, program);

        var folder = gui.addFolder('Block ' + b);
        folder.add(program, 'Retail', 0, max).step(1000).onFinishChange(updateProgram);
        folder.add(program, 'Residential', 0, max).step(1000).onFinishChange(updateProgram);
        folder.add(program, 'Office', 0, max).step(1000).onFinishChange(updateProgram);
        folder.add(program, 'Hotel', 0, max).step(1000).onFinishChange(updateProgram);
        folder.add(program, 'Education', 0, max).step(1000).onFinishChange(updateProgram);
        folder.add(program, 'Parking', 0, 500).step(10).onFinishChange(updateProgram);
    });

    function updateProgram(bp, value) {
        //updated program stored in newProgram map
        //existing programs in programsByBlock

        newProgram.forEach(function (block, value) {
            bp.pTableByBlock.filter(block);
            bp.pTableByProgram.filter('Retail').top(Infinity)[0].v = value.Retail;
            bp.pTableByProgram.filter('Residential').top(Infinity)[0].v = value.Residential;
            bp.pTableByProgram.filter('Office').top(Infinity)[0].v = value.Office;
            bp.pTableByProgram.filter('Hotel').top(Infinity)[0].v = value.Hotel;
            bp.pTableByProgram.filter('Education').top(Infinity)[0].v = value.Education;
            bp.pTableByProgram.filter('Parking').top(Infinity)[0].v = value.Parking;
        });

        redraw();
    }
}

function parse(bp, d) {
    var block = d.Block;
    delete d.Block;

    if (!bp.blockToId.get(block)) {
        bp.blockToId.set(block, b);
        b++;
    }

    for (key in d) {
        bp.programsByBlock.push({
            block: block,
            program: key,
            v: +d[key]
        });

        if (programToId.get(key) == undefined) {
            programToId.set(key, p);
            p++;
        }
        else {
            console.log("defined");
        }
    }
}

